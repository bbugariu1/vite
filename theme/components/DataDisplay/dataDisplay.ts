import { Components, Theme } from "@mui/material/styles";
import ThemeDivider from "./Divider/ThemeDivider";
import ThemeTable from "./Table/ThemeTable";

/**
 * @see https://mui.com/customization/default-theme/ <i>Data Display</i> section for a list of all components grouped under the Data Display list
 * @param theme
 */
const dataDisplay = (theme: Theme): Components => {
    return {
        ...ThemeDivider(theme),
        ...ThemeTable(theme)
    };
};

export default dataDisplay;