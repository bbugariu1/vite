import { Components, Theme } from "@mui/material/styles";

const ThemeTable = (theme: Theme): Components => {
    return {
        MuiTable: {
            defaultProps: {
                size: "small"
            },
            styleOverrides: {
                root: {
                    // border: "1px solid red"
                }
            }
        },
        MuiTableHead: {
            styleOverrides: {
                root: {
                    backgroundColor: theme.palette.primary[50]
                }
            }
        },
        MuiTableBody: {
            styleOverrides: {
                root: {
                    // border: "1px solid red"
                }
            }
        },
        MuiTableCell: {
            styleOverrides: {
                root: {
                    border: `1px solid ${theme.palette.primary[100]}`
                },
                sizeSmall: {
                    ...theme.typography.body2
                }
            }
        }
    };
};

export default ThemeTable;