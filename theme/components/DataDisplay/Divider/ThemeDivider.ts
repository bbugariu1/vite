import { Components, Theme } from "@mui/material/styles";

const ThemeDivider = (theme: Theme): Components => {
    return {
        MuiDivider: {
            styleOverrides: {
                light: {
                    backgroundColor: theme.palette.primary.light
                }
            }
        }
    };
};

export default ThemeDivider;