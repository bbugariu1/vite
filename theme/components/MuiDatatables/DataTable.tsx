import React from "react";
import MUIDataTable, { MUIDataTableColumnDef, MUIDataTableOptions } from "mui-datatables";
import { Box } from "@mui/material";
import CustomSearchRender from "./CustomSearchRender";
import useStyles from "./styles";

interface IProps {
    title: string;
    options: MUIDataTableOptions;
    columns: MUIDataTableColumnDef | any;
    data: Array<object | number[] | string[]>;
}

/**
 * Default theme DataTable component https://github.com/gregnb/mui-datatables
 *
 * @param title
 * @param options
 * @param columns
 * @param data
 * @constructor
 */
const DataTable = ({ title, options, columns, data }: IProps) => {
    const styles = useStyles();
    const [defaultOptions, setDefaultOptions] = React.useState<MUIDataTableOptions>({
        search: true,
        download: false,
        print: false,
        viewColumns: false,
        filter: false,
        elevation: 0,
        filterType: "dropdown",
        responsive: "vertical",
        selectableRows: "none",
        setTableProps: () => ({
            // padding: "checkbox"
        }),
        draggableColumns: { enabled: true },
        customSearchRender: (searchText, handleSearch, hideSearch, options) => (
            <CustomSearchRender
                searchText={searchText}
                onSearch={handleSearch}
                onHide={hideSearch}
                options={options}
            />
        )
    });

    React.useEffect(() => {
        const overridesOptions = { ...defaultOptions, ...options };
        setDefaultOptions(overridesOptions);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [options]);

    return (
        <Box sx={styles.root}>
            <MUIDataTable
                title={title}
                data={data}
                columns={columns}
                options={defaultOptions}
            />
        </Box>
    );
};

export default DataTable;