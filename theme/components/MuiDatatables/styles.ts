import { useTheme } from "@mui/material";
import { SxProps } from "@mui/system";

const useStyles = () => {
    const theme = useTheme();

    const root: SxProps = {
        "& .MuiTypography-root": {
            ...theme.typography.body1
        },
        "*": {
            ...theme.typography.body2
        },
        "> div": {
            "> div": {
                "table": {
                    "thead": {
                        "th": {
                            borderLeft: "none",
                            borderBottom: "none",
                            "&:last-child": {
                                borderRight: "none"
                            },
                            "div": {
                                fontWeight: theme.typography.fontWeightBold
                            }
                        }
                    },
                    "tbody": {
                        "td": {
                            borderLeft: "none",
                            borderBottom: "none",
                            "&:last-child": {
                                borderRight: "none"
                            }
                        }
                    }
                }
            }
        },
        "tfoot": {
            "td": {
                borderLeft: "none",
                borderRight: "none",
                borderBottom: "none",
                height: "20px"
            }
        }
    };

    return {
        root
    };
};

export default useStyles;