import React from "react";
import Grow from "@mui/material/Grow";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import ClearIcon from "@mui/icons-material/Clear";
import { withStyles } from "@mui/styles";
import { Theme } from "@mui/material";

const defaultSearchStyles = (theme: Theme) => ({
    main: {
        display: "flex",
        flex: "1 0 auto"
    },
    searchText: {
        flex: "0.8 0"
    },
    clearIcon: {
        "&:hover": {
            color: theme.palette.error.main
        }
    }
});

interface IProps {
    searchText: string,
    onSearch: (text: string) => void;
    onHide: () => void;
    options: any;
    classes: any;
}

const CustomSearchRender = ({ searchText, onSearch, onHide, options,classes }: IProps) => {
    const rootRef = React.useRef(null);
    const searchFieldRef = React.useRef(null);

    const handleTextChange = (event: React.SyntheticEvent | any) => {
        onSearch(event.target.value);
    };

    return (
        <Grow appear in={true} timeout={300}>
            <div className={classes.main} ref={rootRef}>
                <TextField
                    className={classes.searchText}
                    InputProps={{
                        "aria-label": options.textLabels.toolbar.search
                    }}
                    value={searchText || ""}
                    onChange={handleTextChange}
                    fullWidth={true}
                    inputRef={searchFieldRef}
                />
                <IconButton className={classes.clearIcon} onClick={onHide}>
                    <ClearIcon/>
                </IconButton>
            </div>
        </Grow>
    );
};

export default withStyles(defaultSearchStyles, { name: 'CustomSearchRender' })(CustomSearchRender);