import { alpha, Components, Theme } from "@mui/material";
import { themeMetrics } from "../../../index";

const ThemeOutlinedInput = (theme: Theme): Components => {
    return {
        MuiOutlinedInput: {
            defaultProps: {},
            styleOverrides: {
                root: {
                    '&.Mui-focused': {
                        boxShadow: `${alpha(theme.palette.tertiary.main, 0.25)} 0 0 0 0.2rem`,
                        "& .MuiOutlinedInput-notchedOutline" : {
                            border: `1px solid ${theme.palette.tertiary.main}`
                        }
                    }
                },
                input: {
                    minHeight: themeMetrics.inputs.baseHeight,
                    paddingTop: 0,
                    paddingBottom: 0
                },
                sizeSmall: {
                    height: themeMetrics.inputs.baseSmallHeight,
                    ...theme.typography.subtitle2
                }
            }
        }
    };
};

export default ThemeOutlinedInput;