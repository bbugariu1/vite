import { alpha, Components, Theme } from "@mui/material";

const ThemeInputBase = (theme: Theme): Components => {
    return {
        MuiInputBase: {
            defaultProps: {},
            styleOverrides: {
                root: {
                    ...theme.typography.subtitle2,
                    "&.Mui-focused": {
                        boxShadow: `${alpha(theme.palette.tertiary.main, 0.25)} 0 0 0 0.2rem`,
                        "& .MuiOutlinedInput-notchedOutline": {
                            border: `1px solid ${theme.palette.tertiary.main}`
                        }
                    }
                },
                inputMultiline: {
                    // resize: "both"
                }
            }
        },
        MuiInputLabel: {
            styleOverrides: {
                root: {
                    ...theme.typography.body2,
                    color: theme.palette.primary.main
                }
            }
        },
        MuiFormHelperText: {
            styleOverrides: {
                root: {
                    marginLeft: 0
                }
            }
        }
    };
};

export default ThemeInputBase;