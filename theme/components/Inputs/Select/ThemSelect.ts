import { Components, Theme } from "@mui/material";
import { themeMetrics } from "../../../index";

const ThemeSelect = (theme: Theme): Components => {
    return {
        MuiSelect: {
            defaultProps: {},
            styleOverrides: {
                select: {
                    minHeight: themeMetrics.inputs.baseHeight,
                    lineHeight: themeMetrics.inputs.baseHeight
                }
            }
        }
    };
};

export default ThemeSelect;