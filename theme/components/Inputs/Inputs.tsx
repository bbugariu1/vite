import { Components, Theme } from "@mui/material/styles";
import ThemeButton from "./Button/ThemeButton";
import ThemeInputBase from "./TextField/ThemeInputBase";
import ThemeOutlinedInput from "./TextField/ThemeOutlinedInput";
import ThemeSelect from "./Select/ThemSelect";
import ThemeCheckbox from "./Checkbox/ThemeCheckbox";
import ThemeAutocomplete from "./Autocomplete/ThemeAutocomplete";

/**
 * @see https://mui.com/customization/default-theme/ <i>Inputs</i> section for a list of all components grouped under the Inputs list
 * @param theme
 */
const inputs = (theme: Theme): Components => {
    return {
        ...ThemeButton(theme),
        ...ThemeInputBase(theme),
        ...ThemeOutlinedInput(theme),
        ...ThemeSelect(theme),
        ...ThemeCheckbox(theme),
        ...ThemeAutocomplete(theme)
    };
};

export default inputs;