import { Components, Theme } from "@mui/material/styles";
import { themeMetrics } from "../../../index";

/**
 * Overwrite default MuiButton component
 *
 * @see https://mui.com/api/autocomplete/
 * @param theme
 * @constructor
 */
const ThemeAutocomplete = (theme: Theme): Components => {
    return {
        MuiAutocomplete: {
            styleOverrides: {
                root: {
                    padding: 0,
                    "*": {
                        padding: 0,
                    }
                },
                popper: {

                },
                input: {
                    padding:0,
                },
                inputRoot: {
                    padding: 0,
                    minHeight: themeMetrics.inputs.baseHeight
                },
                inputFocused: {
                    padding:"0 0 0 8px !important",
                },

            }
        }
    };
};

export default ThemeAutocomplete;