import { Components, Theme } from "@mui/material/styles";

const ThemeCheckbox = (theme: Theme): Components => {
    return {
        MuiCheckbox: {
            defaultProps: {
                color: "secondary",
                size: "small"
            },
            styleOverrides: {
                root: {
                    padding: theme.spacing(0.5)
                }
            }
        },
        MuiFormControlLabel: {
            styleOverrides: {
                label: {
                    ...theme.typography.body2
                }
            }
        }
    };
};

export default ThemeCheckbox;