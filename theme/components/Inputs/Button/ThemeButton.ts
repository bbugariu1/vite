import { Components, Theme } from "@mui/material/styles";
import { themeMetrics } from "../../../index";

/**
 * Overwrite default MuiButton component
 *
 * @see https://mui.com/api/button/
 * @param theme
 * @constructor
 */
const ThemeButton = (theme: Theme): Components => {
    return {
        MuiButtonBase: {
            defaultProps: {
                disableRipple: true
            }
        },
        MuiButton: {
            defaultProps: {
                disableRipple: true,
                disableElevation: true,
                variant: "contained"
            },
            styleOverrides: {
                root: {
                    minHeight: themeMetrics.inputs.baseHeight,
                    textTransform: "capitalize",
                    ...theme.typography.body1
                },
                sizeSmall: {
                    minHeight: themeMetrics.inputs.baseSmallHeight,
                    ...theme.typography.subtitle2,
                    borderWidth: "2px",
                    "&:hover": {
                        borderWidth: "2px",
                    }
                }
            }
        }
    };
};

export default ThemeButton;