import * as React from "react";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import Paper, { PaperProps } from "@mui/material/Paper";
import Draggable from "react-draggable";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import theme from "../../../index";
import { Box, Grid } from "@mui/material";
import { Breakpoint } from "@mui/system";

function PaperComponent (props: PaperProps) {
    return (
        <Draggable
            handle="#draggable-dialog-title"
            cancel={"[class*=\"MuiDialogContent-root\"]"}
        >
            <Paper {...props} />
        </Draggable>
    );
}

export interface DialogTitleProps {
    id?: string;
    children: React.ReactNode;
    onClose?: () => void;
}

export interface DialogProps {
    children?: React.ReactElement | React.ReactElement[];
    open: boolean;
    title?: string | React.ReactNode;
    fullWidth?: boolean;
    setIsOpen: (prevState: boolean) => void;
    onClose?: (prevState: boolean) => void;
    maxWidth?: Breakpoint;
}

const CustomDialogTitle = (props: DialogTitleProps) => {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle style={{ cursor: "move" }} {...other} sx={{
            m: 0,
            pr: theme.spacing(0.5),
            pb: theme.spacing(1),
            pt: theme.spacing(0.5),
            color: theme.palette.common.white
        }}>
            {onClose ? (
                <Grid container justifyContent="flex-end" alignItems="center" spacing={1}>
                    <Grid item>
                        <Typography variant="caption" sx={{ lineHeight: "40px" }}>
                            Close
                        </Typography>
                    </Grid>
                    <Grid item>
                        <IconButton aria-label="close" onClick={onClose} sx={{
                            width: theme.spacing(3.5),
                            height: theme.spacing(3.5),
                            background: theme.palette.common.white
                        }}>
                            <CloseIcon/>
                        </IconButton>
                    </Grid>
                </Grid>
            ) : null}
        </DialogTitle>
    );
};

const OptionDialogTitle = (props: DialogTitleProps): React.ReactElement => {
    const { children } = props;
    return (
        <Box sx={{
            background: theme.palette.background.paper,
            color: theme.palette.primary.dark,
            p: theme.spacing(3),
            borderRadius: "5px 5px 0 0"
        }}>
            <Typography variant="body1" sx={{ fontWeight: theme.typography.fontWeightBold }}>
                {children}
            </Typography>
        </Box>
    );
};

/**
 * Main theme Dialog
 *
 * @param children
 * @param title
 * @param open
 * @param setIsOpen
 * @param onClose
 * @param fullWidth
 * @param maxWidth
 * @constructor
 */
const CustomDialog = ({ children, title, open, setIsOpen, onClose, fullWidth, maxWidth }: DialogProps): React.ReactElement => {
    const handleContextMenu = (event: React.MouseEventHandler<HTMLDivElement> | React.SyntheticEvent | any): void => {
        event.stopPropagation();
    };

    return (
        <Dialog
            onClose={onClose}
            open={open}
            fullWidth={!!fullWidth}
            maxWidth={maxWidth ? maxWidth : "sm"}
            onContextMenu={handleContextMenu}
            PaperComponent={PaperComponent}
            aria-labelledby="draggable-dialog-title"
            sx={{
                [`& .MuiDialog-paper`]: {
                    background: "transparent",
                    boxShadow: "none"
                }
            }}
        >
            <CustomDialogTitle id="draggable-dialog-title" onClose={() => setIsOpen(!open)}>
                {/*  no text here as per design specifications  */}
            </CustomDialogTitle>
            <Paper sx={{ backgroundColor: "background.default" }}>
                {title
                    ?
                    <OptionDialogTitle>
                        {title}
                    </OptionDialogTitle>
                    : null
                }
                {children}
            </Paper>
        </Dialog>
    );
};

export default CustomDialog;