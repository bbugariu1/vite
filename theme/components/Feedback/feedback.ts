import { Components, Theme } from "@mui/material/styles";

/**
 * @see https://mui.com/customization/default-theme/ <i>Feedback</i> section for a list of all components grouped under the Feedback list
 * @param theme
 */
const feedback = (theme: Theme): Components => {
    return {
        // TODO: add custom Feedback components style
    }
};

export default feedback;