import { Theme, ThemeOptions, createTheme } from "@mui/material/styles";
import inputs from "./components/Inputs/Inputs";
import dataDisplay from "./components/DataDisplay/dataDisplay";
import { ColorPartial } from "@mui/material/styles/createPalette";

declare module "@mui/material/styles" {
    interface Palette {
        tertiary: Palette["primary"] | any;
    }

    interface PaletteColor extends ColorPartial {}

    interface PaletteOptions {
        tertiary: PaletteOptions["primary"];
    }
}

/**
 * Define default elements width/height metrics
 */
export type ThemeMetrics = {
    inputs: {
        baseHeight: string;
        baseMediumHeight: string;
        baseSmallHeight: string;
    },
    buttons: {

    }
}

export const themeMetrics: ThemeMetrics = {
    inputs: {
        baseHeight: "2.75rem",
        baseMediumHeight: "2rem",
        baseSmallHeight: "1.375rem"
    },
    buttons: {

    }
};

/**
 * The default light theme
 */
const themeOptions: ThemeOptions = {
    palette: {
        mode: "light",
        background: {
            default: "#FBFBFB"
        },
        primary: {
            main: "#2D353A",
            dark: "#040E13",
            light: "#575F64",
            "900": "#2D353A",
            "800": "#404A52",
            "700": "#4F5D67",
            "600": "#60717D",
            "500": "#6B808E",
            "400": "#82939F",
            "300": "#99A7B0",
            "200": "#B7C0C7",
            "100": "#D5D9DB",
            "50": "#EFEFEF"
        },
        secondary: {
            main: "#98BF0D",
            dark: "#658E00",
            light: "#CCF24D",
            "900": "#346800",
            "800": "#5B8902",
            "700": "#719C02",
            "600": "#87AF02",
            "500": "#98BF0D",
            "400": "#A9C842",
            "300": "#B9D367",
            "200": "#CDDE94",
            "100": "#E2ECBF",
            "50": "#F3F7E5"
        },
        tertiary: {
            main: "#2196F3",
            dark: "#0067C0",
            light: "#6EC4FF",
            "900": "#0D47A1",
            "800": "#1565C0",
            "700": "#1976D2",
            "600": "#1E88E5",
            "500": "#2196F3",
            "400": "#42A5F5",
            "300": "#64B5F6",
            "200": "#90CAF9",
            "100": "#BBDEFB",
            "50": "#E3F2FD"
        }
    },
    typography: {
        htmlFontSize: 16,
        fontSize: 14,
        allVariants: {
            // color: "#000000"
        },
        h1: {
            fontSize: "6rem"
        },
        h2: {
            fontSize: "3.75rem"
        },
        h3: {
            fontSize: "3rem"
        },
        h4: {
            fontSize: "2.125rem"
        },
        h5: {
            fontSize: "1.5rem"
        },
        h6: {
            fontSize: "1.25.rem"
        },
        subtitle1: {
            fontSize: "1rem"
        },
        subtitle2: {
            fontSize: "0.875rem"
        },
        body1: {
            fontSize: "1rem"
        },
        body2: {
            fontSize: "0.875rem"
        },
        button: {
            fontSize: "0.875rem",
            fontWeight: 600
        },
        caption: {
            fontSize: "0.75rem"
        },
        overline: {
            fontSize: "0.625rem"
        }
    }
};

const theme: Theme = createTheme(themeOptions);

theme.components = {
    ...inputs(theme),
    ...dataDisplay(theme),
};

export default theme;