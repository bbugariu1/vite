import React from "react";
import ReactDOM from "react-dom";
import App from "./app/App";
import { CssBaseline, ThemeProvider } from "@mui/material";
import theme from "../theme";
import { Provider } from "react-redux";
import store from "./store";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";

let persistor = persistStore(store);

ReactDOM.render(
    <React.StrictMode>
        <CssBaseline />
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <PersistGate loading={null} persistor={persistor}>
                    <App/>
                </PersistGate>
            </ThemeProvider>
        </Provider>
    </React.StrictMode>,
    document.getElementById("root")
);
