import { ReactNode, useLayoutEffect, useState } from "react";
import { Navigate, useLocation } from "react-router-dom";
import { useAppSelector } from "../store/hooks";
import PublicLayout from "../layouts/PublicLayout";

interface IProps
{
    children: ReactNode;
}

const AppRoute = ({ children }: IProps) =>
{
    const user = useAppSelector(state => state.user);
    const location = useLocation();
    const [publicRoutes] = useState<string[]>(["/login"]);
    const isPrivate = !publicRoutes.includes(location.pathname);


    if (!user.authenticated && isPrivate) {
        // Redirect them to the /login page, but save the current location they were
        // trying to go to when they were redirected. This allows us to send them
        // along to that page after they login, which is a nicer user experience
        // than dropping them off on the home page.
        return <Navigate to={"/login"} state={{ from: location }} replace/>;
    }

    if (!isPrivate) {
        return (
            <PublicLayout>
                {children}
            </PublicLayout>
        );
    }

    return (
        <>
            {children}
        </>
    );
};

export default AppRoute;