import { FC } from "react";

const ROUTES = import.meta.globEager("../features/**/pages/**/*.tsx");

const routes = Object.keys(ROUTES).map((path: string): { name: string; path: string; component: FC } =>
{
    const groups = path.match(/^..\/features\/(.*)\/pages(.*).tsx$/) as string[];
    const component = ROUTES[path].default;
    const parts = groups
        .slice(1, groups.length)
        .filter((name: string) => name.replace("/", "") !== "index");

    let name = parts.join("")
        .replace(/\[\.{3}.+\]/, "*")
        .replace(/\[(.+)\]/, ":$1");

    path = name === "home" ? "/" : `/${name.toLowerCase()}`;

    if (name === "user/login") {
        path = "login";
    }

    return { name, path, component };
});

export default routes;