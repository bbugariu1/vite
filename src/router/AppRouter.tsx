import { Link, Route, Routes } from "react-router-dom";
import routes from "./routes";
import AppRoute from "./AppRoute";
import PageNotFound from "./404";

const AppRouter = () =>
{
    return (
        <>
            <ul style={{ position: "fixed", bottom: 0, border: "4px dotted green" }}>
                {routes.map(({ name, path }) =>
                {
                    return (
                        <li key={path}>
                            <Link to={path}>{name}</Link>
                        </li>
                    );
                })}
            </ul>
            <>
                <Routes>
                    {routes.map(route =>
                    {
                        return <Route
                            key={route.path}
                            path={route.path}
                            element={(
                                <AppRoute>
                                    <route.component/>
                                </AppRoute>
                            )}/>;
                    })}

                    <Route path="*" element={<PageNotFound/>}/>
                </Routes>
            </>
        </>
    );
};

export default AppRouter;