import { Box, Container, Grid, Typography } from "@mui/material";

const PageNotFound = () =>
{
    return (
        <Grid container justifyContent="center" alignItems="center" sx={{ height: "100vh" }}>
            <Grid item>
                <Typography variant={"h1"} sx={{color: "primary.light"}}>404</Typography>
            </Grid>
        </Grid>
    );
};

export default PageNotFound;