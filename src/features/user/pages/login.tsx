import { Paper } from "@mui/material";
import LoginForm from "../components/LoginForm";
import { styled } from "@mui/system";

const Wrapper = styled("div")(({ theme }) => ({
    border: "1px solid red",
    display: "flex",
    height: "100vh",
    justifyContent: "center",
    alignItems: "center",
}));

const Main = styled("main")(({ theme }) => ({
    width: "521px"
}));

const LoginPage = () =>
{
    return (
        <Wrapper>
            <Main>
                <LoginForm/>
            </Main>
        </Wrapper>
    );
};

export default LoginPage;