import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface IUserState
{
    authenticated: boolean;
}

const initialState: IUserState = {
    authenticated: false
};
const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        authenticated: (state, payload: PayloadAction<boolean>) =>
        {
            state.authenticated = false;
        }
    }
});

export default userSlice