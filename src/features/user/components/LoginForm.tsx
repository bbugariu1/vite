import { useFormik } from "formik";
import * as yup from "yup";
import { Box, Button, Grid, InputLabel, Paper, TextField, Theme, Typography, useTheme } from "@mui/material";
import { styled } from "@mui/system";

interface IValues
{
    username: string;
    password: string;
}

const initialValues: IValues = {
    username: "",
    password: ""
};

const validationSchema = yup.object({
    username: yup
        .string()
        .required("Username is required"),
    password: yup
        .string()
        .min(8, "Password should be of minimum 8 characters length")
        .required("Password is required")
});

const Form = styled("form")(({ theme }: { theme: Theme }) => ({
    padding: theme.spacing(4),
    borderRadius: theme.shape.borderRadius
}));

const LoginForm = () =>
{
    const theme = useTheme();
    const formik: any = useFormik({
        initialValues,
        validationSchema,
        onSubmit: (values) =>
        {
            alert(JSON.stringify(values, null, 2));
        }
    });

    return (
        <Paper elevation={4}>
            <Form onSubmit={formik.handleSubmit} theme={theme}>
                <Grid container direction="column" gap={2}>
                    <Grid item xs={12}>
                        <Typography variant="h3">Login</Typography>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Grid container alignItems="center">
                            <Grid item xs={12} md={3}>
                                <InputLabel htmlFor="username">Username</InputLabel>
                            </Grid>
                            <Grid item xs={12} md={9}>
                                <TextField
                                    fullWidth
                                    id="username"
                                    name="username"
                                    value={formik.values.username}
                                    onChange={formik.handleChange}
                                    error={formik.touched.username && Boolean(formik.errors.username)}
                                    helperText={formik.touched.username && formik.errors.username}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Grid container alignItems="center">
                            <Grid item xs={12} md={3}>
                                <InputLabel htmlFor="password">Password</InputLabel>
                            </Grid>
                            <Grid item xs={12} md={9}>
                                <TextField
                                    fullWidth
                                    id="password"
                                    name="password"
                                    type="password"
                                    value={formik.values.password}
                                    onChange={formik.handleChange}
                                    error={formik.touched.password && Boolean(formik.errors.password)}
                                    helperText={formik.touched.password && formik.errors.password}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} md={6} justifyContent={"right"}>
                        <Button color={"secondary"} variant="contained" type="submit">
                            Log in
                        </Button>
                    </Grid>
                </Grid>
            </Form>
        </Paper>
    );
};

export default LoginForm;