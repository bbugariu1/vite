import { Container } from "@mui/material";
import { FC, ReactNode } from "react";

interface IProps
{
    children?: ReactNode
}

const PublicLayout: FC = ({ children }: IProps) =>
{
    return (
        <>
            {children}
        </>
    );
};

export default PublicLayout;